# Django REST API with PostgreSQL: A CRUD App

sample from  [Django Rest Framework with PostgreSQL: A CRUD Tutorial](https://jkaylight.medium.com/django-rest-framework-with-postgresql-a-crud-tutorial-8a34283e9c12).

### To Run on Localhost

Create a virtual environment for the project:

```bash
python3 -m venv venv
```

Then, activate it:

```bash
source venv/bin/activate
```

Install Django and other packages:

```bash
pip install -r requirements.txt
```

migrate data

```bash
python manage.py migrate
```

run server
```bash
python manage.py runserver
```

**Spot on!**

http://localhost:8000/customer/create for adding data

http://localhost:8000/customer/{id} for get detail data

http://localhost:8000/customer/ get all data customer


## run on docker

go to crm_project set the environment of database 

docker build -t sample-django .

docker run -d -p 1222:8000 sample-django 